#include "stdafx.h"
using namespace std;

//���������� �����, ������� � ���������
#define k 4

//��������� �����
#define free true
#define busy false

//��������� �������
void Initialize();
void ChangeForksState(int forkNumber, bool valueState);
void ThreadFunction(int PhilosopherId);
bool CheckForks(int PhilosopherId);
void print(string text);
byte GetFirstForkNumberByPhilosopherId(int n);
byte GetSecondForkNumberByPhilosopherId(int n);

//���� - �������� ���������� ���������
fstream protocol("protocol.txt", ios_base::out | ios_base::trunc);

/*��������� ������ "�������"*************************************************************************************************************************/
//��������� ��������
enum state {
	wait//�����
	, eat//������
	, think//���������������
};

//�������� "�������"
struct philosopher{
	int MaxEnergy;//������� ���� ������, ����� ����������
	int CurrentEnergy;//������� ������� ���������
	state State;//��������� �������� ( ���� �� ����.: ������, ������, �����)
};

/*GLOBAL OBJECTS*********************************************************************************************************************************/
// -������ ����� (true - ���� ����� ��������, false - ����� ������)
bool Forks[k];

// - ������ ���������
philosopher Philosophers[k];

mutex MessageMutex, ForkMutex;

/*EVENTS OBJECTS*********************************************************************************************************************************/
//������� ������ ���� �����
HANDLE StartEvent = CreateEvent(
	NULL,// ������� ������
	TRUE,// ��� ������ TRUE - ������
	FALSE,// ��������� ��������� TRUE - ����������
	NULL// ��� ������� = Event
	);

//������� ������������ ��� ������ �����
HANDLE ForkIsFreeEvent[k];

/*MAIN FUNCTION**************************************************************************************************************************************/
int _tmain(int argc, _TCHAR* argv[])
{
	Initialize();
	for (int i = 0; i < k; i++)
	{
		thread thr(ThreadFunction, i);
		thr.detach();
	}
	SetEvent(StartEvent);
	cout << "All threads are started!" << endl << "Press any key to stop execution threads" << endl;
	_getche();
	protocol.close();
	return 0;
}

/*THREAD FUNCTION******************************************************************************************************************************/
void ThreadFunction(int PhilosopherId)
{
	//���� �������, ����� ������ �������� �������� (��� �������������� ������ ���� �����)
	WaitForSingleObject(StartEvent, INFINITE);
	while (true)
	{
		switch (Philosophers[PhilosopherId].State)
		{
		case wait:
		{
			print("Philosopher # " + to_string(PhilosopherId) + " is waiting");

			//���� ����� �� ��������
			if (!CheckForks(PhilosopherId))
			{
				//�� ���� ������������ ������ �����
				WaitForSingleObject(ForkIsFreeEvent[GetFirstForkNumberByPhilosopherId(PhilosopherId)], INFINITE);

				//� ������
				WaitForSingleObject(ForkIsFreeEvent[GetSecondForkNumberByPhilosopherId(PhilosopherId)], INFINITE);
			}

			//����������� ������ �1
			ForkMutex.lock();
				if (CheckForks(PhilosopherId))
					ChangeForksState(PhilosopherId, busy);
			ForkMutex.unlock();

			//������ ���������
			Philosophers[PhilosopherId].State = eat;
		}
		break;
		case eat:
		{
			print("Philosopher # " + to_string(PhilosopherId) + " is eating");

			while (Philosophers[PhilosopherId].CurrentEnergy < Philosophers[PhilosopherId].MaxEnergy)
			{
				Philosophers[PhilosopherId].CurrentEnergy++;
				Sleep(10);
			}

			//����������� ������ �2
			ForkMutex.lock();
				ChangeForksState(PhilosopherId, free);
			ForkMutex.unlock();

			Philosophers[PhilosopherId].State = think;
		}
		break;
		case think:
		{
			print("Philosopher # " + to_string(PhilosopherId) + " is thinking");

			while (Philosophers[PhilosopherId].CurrentEnergy > 0)
			{
				Sleep(10);
				Philosophers[PhilosopherId].CurrentEnergy--;
			}
			Philosophers[PhilosopherId].State = wait;
		}
		break;
		}
	}
}

/*ADDITIONAL FUNCTIONS******************************************************************************************************************************/
///������������� ����� ������� ������
void Initialize()
{
	for (int i = 0; i < k; i++)
	{
		Philosophers[i].CurrentEnergy = 0;
		Philosophers[i].MaxEnergy = 10 + (rand() % 91);
		Philosophers[i].State = wait;
		Forks[i] = free;
		ForkIsFreeEvent[i] = CreateEvent(NULL, TRUE, TRUE, NULL);
	}
}

//�������, �������� ������ ����� ��� �������� ��������
void ChangeForksState(int philosopherId, bool value)
{
	//��������� ������� ������ � ������ ����� ��� ��������� ��������
	int firstFork = GetFirstForkNumberByPhilosopherId(philosopherId);
	int secondFork = GetSecondForkNumberByPhilosopherId(philosopherId);

	//���� ����� �������������, �� ������������� �� ����
	if (value == free)
	{
		SetEvent(ForkIsFreeEvent[firstFork]);
		SetEvent(ForkIsFreeEvent[secondFork]);
	}
	//� ���� ����������, �� ���������� ��������������� �������
	else
	{
		ResetEvent(ForkIsFreeEvent[firstFork]);
		ResetEvent(ForkIsFreeEvent[secondFork]);
	}

	//��������� ����� ��������� ������� �����
	string stateString = value ? "freed" : "busy";

	//������ ������ �����
	Forks[firstFork] = Forks[secondFork] = value;

	//������� ��������� �� ����
	print("Fork # " + to_string(firstFork) + " was " + stateString + '\n' + "Fork # " + to_string(secondFork) + " was " + stateString);
}

//�������, ����������� �����
bool CheckForks(int PhilosopherId) { return (Forks[GetFirstForkNumberByPhilosopherId(PhilosopherId)] && Forks[GetSecondForkNumberByPhilosopherId(PhilosopherId)]); }

//������� ����������� ����� ������ ����� ��� n-�� ��������
byte GetFirstForkNumberByPhilosopherId(int n) { return n; }

//������� ����������� ����� ������ ����� ��� n-�� ��������
byte GetSecondForkNumberByPhilosopherId(int PhilosopherId) { return PhilosopherId > 0 ? PhilosopherId - 1 : k - 1; }

//������� ������ � ���� ���������
void print(string text)
{
	//����������� ������ �3
	MessageMutex.lock();
		protocol << text << endl;
	MessageMutex.unlock();
}